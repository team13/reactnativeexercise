import React from 'react';
import { Text, View, Image, StyleSheet, TouchableOpacity } from 'react-native';

export default class RecipeCard extends React.Component {
    render() {
        return <View style={{ alignItems: 'flex-start' }}>
            <Text>{this.props.plannedMeal.meal_type}</Text>
            <View style={{flexDirection: 'row'}}>
                <Image source={{uri: this.props.plannedMeal.recipe.image_url}}
                       style={{width: 100, height: 100}}
                />
                <View style={{width: 200, paddingLeft: 10, justifyContent: 'space-around'}}>
                    <Text>{this.props.plannedMeal.recipe.title}</Text>
                    <Text>Calories: {this.props.plannedMeal.recipe.nutrition_per_serving.calories.quantity}kCal</Text>
                </View>
            </View>
            <TouchableOpacity style={styles.button}>
                <Text>Skip</Text>
            </TouchableOpacity>
        </View>
    }
}
const styles = StyleSheet.create({
    button: {
        alignItems: 'center',
        backgroundColor: '#DDDDDD',
        padding: 10,
        width: 100,
        marginTop: 16,
    },
});