import React from 'react';
import { Text, View } from 'react-native';
import {preloadedState} from "../services/mock_data";
import RecipeCard from "../components/RecipeCard";

export default class DiaryScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = preloadedState;
    }

    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'space-around', alignItems: 'center' }}>
                {/*<Text style={{ marginTop: 50, fontSize: 25 }}>Diary</Text>*/}
                {
                    this.state.plannedMeals.map(pm =>
                        <RecipeCard key={"pm-" + pm.meal_type} plannedMeal={pm} />
                    )
                }
            </View>
        );
    }
}
